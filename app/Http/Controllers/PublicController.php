<?php

namespace App\Http\Controllers;

use App\Mail\AdminMail;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function index(){
        return view('welcome');
    }

    public function contact(){
        return view('contatti');
    }

    public function submit(Request $request){
        $name = $request->input("name");
        $surname = $request->input("surname");
        $email = $request->input("email");
        $message = $request->input("message");

        $contact = compact("name", "surname", "message");
        $adminContact = compact("name", "surname", "email","message");
        
        Mail::to($email)->send(new ContactMail($contact));
        Mail::to("adminblog@mail.it")->send(new AdminMail($adminContact));

        return redirect(route('thankyou'));
    }
    public function thankyou(){
        return view('thankyou');
    }

}
