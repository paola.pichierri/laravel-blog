<?php

namespace App\Http\Controllers;

use App\Http\Requests\TravelRequest;
use App\Models\Travel;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth")->except("showProduct");
    }

    public function showProduct(){
        //dd("ciao");
        $travels=Travel::all();
        return view('prodotti' , compact("travels"));
    }

    public function travelForm(){
        return view('prodotti_form');
    }

    public function travelSubmit(TravelRequest $req){
        $travel = Travel::create(
            [
                'destination'=>$req->input("destination"),
                'company'=>$req->input("company"),
                'price'=>$req->input("price"),
                'description'=>$req->input("description"),
                'img'=>$req->file("img")->store("public/img"), 
            ],
        );
        //dd($travel->all());
        /* $travel = new Travel();
        $travel->destination=$req->input("destination");
        $travel->company=$req->input("company");
        $travel->price=$req->input("price");
        $travel->description=$req->input("description");
        $travel->img=$req->file("img")->store("public/img"); */

        return redirect(route("welcome"))->with("status", "Meta inserita correttamente");
    }

    public function details(Travel $travel){
        return view("prodotti_dettaglio", compact("travel"));
    }

    public function travelEdit(Travel $travel){
        return view("travel_edit", compact("travel"));
    }

    public function travelUpdate(TravelRequest $req , Travel $travel){
        $travel->destination = $req->destination;
        $travel->company = $req->company;
        $travel->price = $req->price;
        $travel->description = $req->description;

        if($req->file("img")){
            $travel->img = $req-> file("img")->store("public/img");
        }
    }
}
