<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TravelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'destination'=>'required',
            'company'=>'required',
            'price'=>'required',
            'description'=>'required',
            'img'=>'required|image',
        ];
    }

    public function messages(){
        return [
            'destination.required'=>'La destinazione è obbligatoria',
            'company.required'=>'Il nome della compagnia è obbligatorio',
            'price.required'=>'Il prezzo è obbligatorio',
        ];
    }
}
