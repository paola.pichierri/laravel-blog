<x-layout> 
    <x-slot name="title">
        Prodotti
    </x-slot>
    <div class="container">
      <div class="row">
          <div class="col-12 text-center">
              <h1 class="display-4 fw-bold tc-main-blue mt-4 mb-4">I nostri pacchetti viaggio</h1>
          </div>
      </div>
  </div>
  
  <section class=" my-5">
    <div class="container">
        <div class="row">
            @foreach ($travels as $travel)
            
                  <div class="col-12 col-md-4">
                    <article class="blog-card shadow">
                      <div class="blog-card__background">
                        <div class="card__background--wrapper">
                          <div class="card__background--main" >
                              <img src="{{Storage::url($travel->img) ?? ""}}" class="img-fluid my-image" alt="">
                            <div class="card__background--layer"></div>
                          </div>
                        </div>
                      </div>
                      <div class="blog-card__head">
                        <span class="date__box">
                          <span class="date__day">11</span>
                          <span class="date__month">JAN</span>
                        </span>
                      </div>
                      <div class="blog-card__info">
                        <h5 class="my-3">{{$travel->destination}}</h5>
                        <h5 class="my-3">{{$travel->price}}</h5>
                        <a href="{{route("details", compact("travel"))}}" class="btn btn--with-icon"><i class="btn-icon fa fa-long-arrow-right"></i>Dettaglio</a>
                      </div>
                    </article>
                  </div>
            @endforeach
        </div>
    </div>

</section>
</x-layout>
{{-- <div class="col-4">
    <div class="card" style="width: 18rem;">
        <img src="https://picsum.photos/{{$product['id']}}" class="card-img-top" alt="">
        <div class="card-body">
          <h5 class="card-title">{{$product['destination']}}</h5>
          <h5 class="card-title">{{$product['company']}}</h5>
          <h5 class="card-title">{{$product['price']}}</h5>
          <a href="{{route('details',['destination'=>$product['destination']])}}" class="btn btn-primary">vai al dettaglio</a>
        </div>
    </div>
</div> --}}
