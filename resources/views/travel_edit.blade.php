<x-layout>
    <x-slot name="title">
        Form
    </x-slot>
    <h1 class="display-2 fw-bold text-center ">Aggiungi il tuo pacchetto viaggio</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
      <header class="header-form d-flex justify-content-center align-items-center">
        <form class="form" method="POST" action="{{route("travels_edit", compact("travel"))}}" enctype="multipart/form-data">
          @csrf
          @method("put") 
            <div class="mb-3">
              <label for="exampleInputText" class="form-label">Destinazione</label>
              <input type="text" class="form-control" name="destination" value={{old('destination')}}>
            </div>
            <div class="mb-3">
                <label for="exampleInputText" class="form-label">Compagnia</label>
                <input type="text" class="form-control" name="company" value={{old('company')}}>
            </div>
            <div class="mb-3">
                <label for="exampleInputText" class="form-label">Prezzo</label>
                <input type="text" class="form-control" name="price" value={{old('price')}}>
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Inserisci un'immagine</label>
                <input type="file" name="img">
            </div>
            <div class="mb-3">
                <label for="">Descrizione</label>
                <textarea name="description" cols="30" rows="10"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </header>
</x-layout>