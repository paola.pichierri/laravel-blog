<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>
        Grazie per averci contattato {{$contact["name"]}}
    </h1>
    <p>Prenderemo in considerazione la tua richiesta molto presto</p>
    <p>Il messaggio che ci hai mandato è il seguente:</p> 
    <p>
        {{$contact["message"]}}
    </p>   
</body>
</html>