<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1 style="color: red; font-size= 400">Ciao Paola! Un nuovo cliente ha cercato di raggiungerti.</h1>
    <h3>L'utente è: {{$adminContact["name"]}} {{$adminContact["surname"]}}</h3>

    <h3>Rispondi il prima possibile.</h3>
    <p>La sua mail è: {{$adminContact["email"]}}</p>
    <p>Ha lasciato il seguente messaggio:</p>
    <p>{{$adminContact["message"]}}</p>
</body>
</html>