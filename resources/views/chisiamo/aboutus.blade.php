<x-layout>
    <x-slot name="title">Chi siamo</x-slot>

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="display-4 fw-bold tc-main-blue mt-4 mb-4">Chi siamo</h1>
            </div>
        </div>
    </div>
    <div class="container-fluid masthead">
        <div class="container h-100">
          <div class="row h-100 align-items-center">
            <div class="col-12 col-md-6">
              <p class="display-4 fw-bold order-2 order-md-1">Vi presento spacetour.com</p>
                <p class="fs-5">Il mio space-travel blog nasce dall’incontro tra una passione viscerale (quella per i viaggi) e una passione che negli ultimi anni ho messo un po’ da parte ma che adesso voglio far rifiorire (quella per lo spazio). </p>
                <p class="fs-5">Mi piace l’idea di raccontare i luoghi, non solo descriverli. Nei miei post non mi limito infatti a elencare le tappe imperdibili di ogni destinazione o i pub più in voga al momento. Mi interessa soffermarmi su qualcosa di più intimo e soggettivo: ad esempio, i ricordi e le sensazioni che ogni pianeta mi ha lasciato, che ci abbia vissuto anni o che li abbi conosciuti per un solo giorno.
                </p>
            </div>
                <div class="col-12 col-md-6 order-1 order-md-2">
                    <img class="img-fluid" src="/media/austro-2.png" alt="">
              </div>
          </div>
        </div>
    </div>
        {{-- <div class="col-12 col-md-6">
            <img src="/media/travel2.png" alt="" style="width: 300" height="300">
        </div> --}}
        {{-- <div class="row">
            <div class="col-12">
                <h2 class="fw-bold">Come possiamo collaborare insieme?</h2>
                <h4>Non esitare a contattarmi se:</h4>
                <ul>
                    <li>
                        Vuoi capire come comunicare un territorio, se hai deciso di intraprendere attività di marketing territoriale per promuoverlo e contribuire al suo sviluppo e alla sua visibilità.
                    </li>
                    <li>
                        Sei <strong>l’Ente del Turismo</strong> di un piccolo comune, di una grande città, di un paese o di un continente e vuoi promuovere la tua destinazione.
                    </li>
                    <li>
                        Hai una <strong>struttura ricettiva, una casa vacanze, un ristorante, un ostello, un B&b</strong> e vuoi sapere quali strumenti utilizzare per comunicare al meglio e attirare clienti.
                    </li>
                    <li>
                        Sei <strong>un’agenzia di viaggi, un tour operator, una start up o un altro tipo di impresa nel settore del turismo e dell’ospitalità</strong> e hai bisogno di un supporto nella gestione della tua presenza online oppure di una consulenza negli ambiti della comunicazione, dei social, del blogging.
                    </li>
                    <li>
                    Lavori per un <strong>ufficio stampa e/o sei un consulente PR</strong> e curi progetti di comunicazione per aziende del turismo.
                    </li>
                </ul>
            </div>
        </div> --}}
        
    </div>

    

 
    
</x-layout>