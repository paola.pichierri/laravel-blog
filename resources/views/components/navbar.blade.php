@auth
    
  <nav class="navbar navbar-expand-lg navbar-light navbar-bg bg-light sticky-top" id="pNavbar">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{route('welcome')}}">
        <img src="/media/newlogo.png" alt="" width="40" height="40" class="d-inline-block align-text">
        spacetour.com
        {{Auth::user()->name}}
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-lg-0">
          <li class="nav-item px-3">
            <a class="nav-link active" aria-current="page" href="{{route('welcome')}}">Home</a>
          </li>
          <li class="nav-item px-3">
            <a class="nav-link" href="{{route('chisiamo')}}">Chi siamo</a>
          </li>
          <li class="nav-item px-3">
            <a class="nav-link" href="{{route('prodotti')}}">Prodotti</a>
          </li>
          <li class="nav-item px-3">
            <a class="nav-link" href="{{route('travels_form')}}">Form prodotti</a>
          </li>
          <li class="nav-item px-3">
            <a class="nav-link" href="{{route('contact')}}">Contatti</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('logout')}}" onclick="event.preventDefault();   document.getElementById('form-logout').submit();">Logout</a>
            <form id="form-logout" action="{{route('logout')}}" method="POST" class="d-none">@csrf</form>
          </li>
        </ul>
        {{-- <form class="d-flex">
          <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn-nav"><i class="fas fa-search"></i></button>
        </form> --}}
      </div>
    </div>
  </nav>

@else

  <nav class="navbar navbar-expand-lg navbar-light navbar-bg bg-light sticky-top" id="pNavbar">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{route('welcome')}}">
        <img src="/media/newlogo.png" alt="" width="40" height="40" class="d-inline-block align-text">
        spacetour.com
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mx-auto mb-lg-0">
          <li class="nav-item px-3">
            <a class="nav-link active" aria-current="page" href="{{route('welcome')}}">Home</a>
          </li>
          <li class="nav-item px-3">
            <a class="nav-link" href="{{route('chisiamo')}}">Chi siamo</a>
          </li>
          <li class="nav-item px-3">
            <a class="nav-link" href="{{route('prodotti')}}">Prodotti</a>
          </li>
          <li class="nav-item px-3">
            <a class="nav-link" href="{{route('travels_form')}}">Form prodotti</a>
          </li>
          <li class="nav-item px-3">
            <a class="nav-link" href="{{route('contact')}}">Contatti</a>
          </li>
          <li class="nav-item px-3">
            <a class="nav-link" href="{{route('register')}}">Registrati</a>
          </li>
          <li class="nav-item px-3">
            <a class="nav-link" href="{{route('login')}}">Login</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

@endauth