<x-layout>
    <x-slot name="title">
        Homepage
    </x-slot>  
            <div class="container-fluid masthead">
                <div class="container h-100">
                  <div class="row h-100 align-items-center">
                    <div class="col-12 col-md-6">
                      <h2 class="display-2 fw-bold order-2 order-md-1">Spacetour.com</h2>
                      <h3>Il blog di viaggi spaziali che cercavi!</h3>
                      <p class="my-4 fst-italic">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi dignissimos beatae aliquam inventore magnam earum doloribus voluptates et iste quidem? Dicta, consectetur non praesentium tempore suscipit aliquid voluptatum sunt nemo?</p>
                      <button class="button-81 btn-81" role="button" onclick="window.location='{{route('prodotti')}}'">Parti con noi!</button>
                    </div>
                    <div class="col-12 col-md-6 order-1 order-md-2">
                      <img class="img-fluid" src="/media/austronauta.png" alt="">
                    </div>
                  </div>
                </div>
              </div>


            {{-- <div class="container-fluid pb-5">
                <div class="container">
                    <div class="row pt-4">
                        <div class="col-12 text-center">
                          <h2 class="display-4 fw-bold tc-main-blue mt-4 mb-4">Ultimi articoli</h2>
                        </div>
                    </div>
                    <div class="row">


                        <div class="col-lg-6 col-md-3 p-0">
                            <div class="inside-item set-bg1 main-pic">
                                <div class="inside-item-text overlay">
                                    <span>08</span>
                                    <p>August</p>
                                    <h5>Lorem ipsum dolor sit amet consectetur adipisicing elit.<br>Quasi explicabo autem ipsa dolor voluptatibus </h5>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-3 col-md-6 p-0">


                            <div class="inside-item inside-item--small set-bg2 main-pic">
                                <div class="inside-item-text overlay">
                                    <span>08</span>
                                    <p>August</p>
                                    <h5>Lorem ipsum dolor sit amet consectetur adipisicing elit.<br>Quasi explicabo autem ipsa dolor voluptatibus </h5>
                                </div>
                            </div>

                                
                            <div class="inside-item inside-item--small set-bg3 main-pic">
                                <div class="inside-item-text overlay">
                                    <span>08</span>
                                    <p>August</p>
                                    <h5>Lorem ipsum dolor sit amet consectetur adipisicing elit.<br>Quasi explicabo autem ipsa dolor voluptatibus </h5>
                                </div>
                            </div>



                        </div>




                        <div class="col-lg-3 col-md-6 p-0">
                            <div class="inside-item set-bg4 main-pic">
                                <div class="inside-item-text overlay">
                                    <span>08</span>
                                    <p>August</p>
                                    <h5>Lorem ipsum dolor sit amet consectetur adipisicing elit.<br>Quasi explicabo autem ipsa dolor voluptatibus </h5>
                                </div>
                            </div>
                        </div>

                        
                    </div>
                </div>
            </div> --}}

            <div class="container-fluid bg-main-gradient">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center">
                            <h2 class="display-4 fw-bold tc-main-blue mt-4 mb-4">I nostri pianeti</h2>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-12 mb-5">
                            <div class="swiper shadow">
                            <!-- Additional required wrapper -->
                                <div class="swiper-wrapper">
                                <!-- Slides -->
                                    <div class="swiper-slide slide1">
                                        <div class="caption">
                                            <h1>Marte</h1>
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit iusto repudiandae aperiam.</p>
                                        </div>
                                        
                                    </div>
                                    <div class="swiper-slide slide2">
                                        <div class="caption">
                                            <h1>Nettuno</h1>
                                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit iusto repudiandae aperiam.</p>
                                        </div>
                                        
                                    </div>
                                    <div class="swiper-slide slide3">
                                        <div class="caption">
                                            <h1>Plutone</h1>
                                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit iusto repudiandae aperiam.</p>
                                        </div>
                                      
                                    </div>
                                </div>
                                <!-- If we need pagination -->
                                <div class="swiper-pagination">

                                </div>

                            </div>
                        </div>
                        <div class="col-md-6 offset-md-3 mb-4">
                            <button class="button-81" role="button" onclick="window.location='{{route('prodotti')}}'">Scopri gli altri pacchetti</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 
          




</x-layout>