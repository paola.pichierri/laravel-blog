<x-layout>
  <x-slot name="title">
    Contatti
  </x-slot>
  <h1 class="display-2 fw-bold text-center ">Contattaci</h1>
  <header class="header-form d-flex justify-content-center align-items-center">
    <form class="form" method="POST" action="{{route('contact.submit')}}">
      @csrf
      <p class="p-form" type="Name:"><input name='name' class="input-form" placeholder="Write your name here.."></input></p>
      <p class="p-form" type="Surname:"><input name="surname" class="input-form" placeholder="Write your surname here.."></input></p>
      <p class="p-form" type="Email:"><input name="email" class="input-form" placeholder="Let us know how to contact you back.."></input></p>
      <p class="p-form" type="Message:"><input name="message" class="input-form" placeholder="What would you like to tell us.."></input></p>
      <button type="submit" class="btn-form">Invia</button>
    </form>
  </header>

</x-layout>

{{-- 
<div class="container">
    <div class="row justify-content-center">
        <div class="col-6">
            <form >
                
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Email address</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" >
                  
                </div>
                <div class="mb-3">
                  <label for="exampleInputName" class="form-label">Nome</label>
                  <input type="text" class="form-control" id="exampleInputName">
                </div>
                <div class="mb-3">
                    <label for="exampleInputCognome" class="form-label">Cognome</label>
                    <input type="text" class="form-control" id="exampleInputCognome" >
                  </div>
                <textarea   cols="30" rows="10"></textarea>
                
              </form>
        </div>
    </div>
</div> --}}