<x-layout>
    <x-slot name="title">
        Registrazione
    </x-slot>
    <h1 class="display-2 fw-bold text-center">Registrati</h1>
    <header class="header-form d-flex justify-content-center align-items-center">
        <form class="form" method="POST" action="{{route("register")}}">
          @csrf
          
            <label for="exampleInputName1" class="p-form">Nome</label>
            <input type="text" name="name" class="input-form">

            <label for="exampleInputEmail1" class="p-form">Email</label>
            <input type="email" name="email" class="input-form">

            <label for="exampleInputPassword1" class="p-form">Password</label>
            <input type="password" name="password" class="input-form">

            <label for="exampleInputPassword1" class="p-form">Conferma password</label>
            <input type="password" name="password_confirmation" class="input-form">

        <button type="submit" class="btn-form">Invia</button>
        </form>
    </header>
</x-layout>