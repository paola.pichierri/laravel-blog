<x-layout>
    <x-slot name="title">
        Login
    </x-slot>
    <h1 class="display-2 fw-bold text-center ">Login</h1>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <header class="header-form d-flex justify-content-center align-items-center">
      <form class="form" method="POST" action="{{route("login")}}">
        @csrf

        <label for="exampleInputEmail1" class="p-form">Email</label>
        <input name="email" class="input-form">

        <label for="exampleInputPassword1" class="p-form">Password</label>
        <input type="password" name="password" class="input-form">

        <button type="submit" class="btn-form">Invia</button>
      </form>
    </header>

</x-layout>