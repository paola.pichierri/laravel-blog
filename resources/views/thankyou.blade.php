<x-layout>
    <x-slot name="title">
        Thank you
    </x-slot>
    <section class="section-thankyou d-flex flex-column justify-content-center align-items-center">
       
                    <h1 class="h1-thanks text-white">
                        Grazie per averci contattato!
                    </h1>
                    <h2 class="h2-thanks text-white">
                        Presto riceverai una risposta dal nostro admin
                    </h2>
                    <button class="button-81" role="button" onclick="window.location='{{route('welcome')}}'">
                        Torna alla home
                    </button>  
            

    </section>
</x-layout>