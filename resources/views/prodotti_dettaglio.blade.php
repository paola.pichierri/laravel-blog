<x-layout>
  <x-slot name="title">
    Dettagli
</x-slot>
<div class="col-4">
  <article class="blog-card">
    <div class="blog-card__background">
      <div class="card__background--wrapper">
        <div class="card__background--main" >
            <img src="{{$travel['img']}}" class="img-fluid" alt="">
          <div class="card__background--layer"></div>
        </div>
      </div>
    </div>
    <div class="blog-card__head">
      <span class="date__box">
        <span class="date__day">11</span>
        <span class="date__month">JAN</span>
      </span>
    </div>
    <div class="blog-card__info">
      <h5>{{$travel['destination']}}</h5>
   
      <p>{{$travel['description']}}</p>
      <a href="{{route('prodotti')}}" class="btn btn--with-icon"><i class="btn-icon fa fa-long-arrow-right"></i>Torna indietro</a>
      <a href="{{route('travels_edit' , compact("travel"))}}" class="btn btn--with-icon"></i>Modifica</a>
    </div>
  </article>
</div>
    
</x-layout>



