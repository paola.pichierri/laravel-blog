const swiper = new Swiper('.swiper', {
    // Optional parameters
    grabCursor: true,
    loop: true,
    autoplay: {
        delay: 4000,
    },
    effect: "creative",
    creativeEffect: {
        prev: {
          shadow: true,
          translate: [0, 0, -400],
        },
        next: {
          translate: ["100%", 0, 0],
        },
      },
          // If we need pagination
      pagination: {
        el: '.swiper-pagination',
      },

      // Navigation arrows
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },

      // And if we need scrollbar
      scrollbar: {
        el: '.swiper-scrollbar',
      },
});

// const navbar = document.querySelector('#pNavbar')

// document.addEventListener('scroll' , () => {

//     if(window.scrollY > 400){
//         navbar.classList.add('bg-light')
//         navbar.classList.remove('bg-transparent')
//     } else {
//         navbar.classList.remove('bg-light')
//         navbar.classList.add('bg-transparent')
//     }
// });
    
  

