<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\WhoareweController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//! rotte controllate dal publiccontroller
Route::get('/', [PublicController::class, "index"])->name('welcome');
Route::get('/contatti',[PublicController::class,'contact'])->name('contact');
Route::get('/contact/thankyou', [PublicController::class, "thankyou"])->name("thankyou");
Route::post('/contatti/submit',[PublicController::class,'submit'])->name('contact.submit');

//!rotte controllate dal productcontroller
Route::get('/prodotti', [ProductController::class, "showProduct"])->name("prodotti");
// rotte parametriche
Route::get('prodotti/{travel}',[ProductController::class,'details'])->name('details');
Route::get('/form', [ProductController::class ,'travelForm'])->name("travels_form");
Route::post('form/submit', [ProductController::class , 'travelSubmit'])->name("prodottiform_submit");
Route::get('/prodotti/edit/{travel}' , [ProductController::class , 'travelEdit'])->name("travels_edit");
Route::put('/prodotti/update/{travel}' , [ProductController::class, "travelUpdate"])->name("travels_update");

//!rotte relative al WhoareweController
Route::get('/chisiamo', [WhoareweController::class , 'whoarewe'])->name('chisiamo');


